<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'The API';//$router->app->version();
});

$router->options('*', function () use ($router) {
    return null;
});



$router->get('signin', 'UserController@signin');
$router->get('test','ApiController@invoke');

$router->group(['middleware' => ['auth']], function () use ($router) {
	$router->get('userdata', 'UserController@getUserData');
	$router->put('user/{id}/changePass','UserController@changePass');
	$router->get('groups', 'GroupController@all');
	$router->get('group/{id}', 'GroupController@one');
	$router->get('sheets', 'SheetController@all');
	$router->get('sheet/{id}', 'SheetController@one');

	$router->post('data', 'DataController@create');
	$router->put('data/{id}', 'DataController@update');

	$router->get('permission-setup', 'PermissionController@setup');
	$router->put('permission-setup', 'PermissionController@save');

	$router->get('group-setup', 'GroupController@setup');
	$router->put('group-setup', 'GroupController@save');

	$router->get('permissions', 'PermissionController@all');
	$router->post('permission', 'PermissionController@create');
	$router->put('permission/{id}', 'PermissionController@update');
	$router->delete('permission/{id}', 'PermissionController@delete');

	$router->get('users', 'UserController@all');
	$router->post('user', 'UserController@create');
	$router->put('user/{id}', 'UserController@update');
	$router->delete('user/{id}', 'UserController@delete');

	$router->get('specs','SpecialityController@all');
	$router->get('spec/{id}','SpecialityController@one');

	$router->group(['middleware' => ['admin']], function () use ($router) {
		$router->post('sheet', 'SheetController@create');
		$router->put('sheet/{id}', 'SheetController@update');
		$router->delete('sheet/{id}', 'SheetController@delete');
		$router->post('user','UserController@create');
		$router->delete('user/{id}','UserController@delete');
		$router->put('user/{id}/setRole/{role_id}','UserController@setRole');
		$router->put('user/{id}/addGroup/{group_id}','UserController@addGroupToUser');
		$router->delete('user/{id}/delGroup/{group_id}','UserController@delGroupFromUser');
		$router->post('spec','SpecialityController@create');
		$router->delete('spec/{id}','SpecialityController@delete');
	});
});
