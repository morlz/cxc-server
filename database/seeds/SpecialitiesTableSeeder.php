<?php

use Illuminate\Database\Seeder;

class SpecialitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('specialities')->insert([
            'code' => '15',
            'name' => 'Программирование в компьютерных системах',
        ]);
		DB::table('specialities')->insert([
            'code' => '12',
            'name' => 'Право и организация социального обеспечения',
        ]);
		DB::table('specialities')->insert([
            'code' => '16',
            'name' => 'Прикладное программирование',
        ]);
    }
}
