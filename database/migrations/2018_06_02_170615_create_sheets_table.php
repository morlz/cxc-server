<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSheetsTable extends Migration
{
    public function up()
    {
        Schema::create('sheets', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('group_id');
			$table->date('from');
			$table->date('to');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sheets');
    }
}
