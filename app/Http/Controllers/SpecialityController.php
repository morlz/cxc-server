<?php

namespace App\Http\Controllers;

use App\Speciality;
use \Illuminate\Http\Request;

class SpecialityController extends Controller
{
	public function all(Request $request) {
		$specs = Speciality::all();
		return response()->json($specs,200);
	}

	public function one(Request $request, $id) {
		$spec = Speciality::where('id',$id)->firstOrFail();
		return response()->json($spec,200);
	}

	public function create(Request $request) {

		$this->validate($request, [
	        'code' => 'required|max:5',
	        'name' => 'required',
    	]);
		$spec = Speciality::create([
			'code' => $request->input('code'),
			'name' => $request->input('name'),
		]);

		return response()->json($spec,200);
	}

	public function delete(Request $request, $id) {
		Speciality::where('id',$id)->delete();
		return response(200);
	}
}
