<?php

namespace App\Http\Controllers;

use App\Permission;
use App\PermissionSetup;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{
    public function __construct()
    {
        //
    }

	public function all (Request $request) {
		$res = Permission::all();
        return response()->json($res, 200);
	}

	public function setup (Request $request) {
		$res = PermissionSetup::all();
        return response()->json($res, 200);
	}

	public function save (Request $request) {
		$items = $request->input('items');
		if (!is_array($items))
			return abort(400, 'Param "items" must be an array');

		PermissionSetup::truncate();
		PermissionSetup::insert($items);

		return response()->json($items, 200);
	}

    public function create (Request $request) {
        $model = Permission::create($request->all());
        $model->save();
        return response()->json($model, 200);
    }

    public function update (Request $request, $id) {
        $model = Permission::findOrFail($id);
        $model->update($request->all());
        return response()->json($model, 200);
    }

	public function delete (Request $request, $id) {
		$model = Permission::findOrFail($id);
		$model->delete();
		return response()->json($model, 200);
	}
}
