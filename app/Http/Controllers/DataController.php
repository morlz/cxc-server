<?php

namespace App\Http\Controllers;

use App\SheetData;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;

class DataController extends Controller
{
    public function __construct()
    {
        //
    }

    public function create (Request $request) {
        $model = SheetData::create($request->all());
        $model->save();
        return response()->json($model, 200);
    }

    public function update (Request $request, $id) {
        $model = SheetData::findOrFail($id);
        $model->update($request->all());
        return response()->json($model, 200);
    }
}
