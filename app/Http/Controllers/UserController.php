<?php

namespace App\Http\Controllers;

use App\User;
use App\Group;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;

class UserController extends Controller
{
    public function __construct()
    {
        //
    }

	public function signin (Request $request) {
        $res = User::where('login', $request->input('login'))
            ->where('pass', $this->hashPass($request->input('pass')))
			->with('permissions')
            ->firstOrFail();

        $res->api_token = $this->hashPass(microtime());
        $res->save();
        $res->makeVisible(["api_token"]);

        return response()->json($res, 200);
    }

	public function changePass(Request $request) {
		if(Auth::user()->id != $request->input('id') || Auth::user()->role_id != 2)
		{
			return response('Foribden',403);
		}

		if($request->input('pass')) {
			$user = User::findOrFail('id', $request->input('id'));
			$user->pass = $this->hashPass($request->input('pass'));
			$user->save();
			return response()->json($user,200);
		}
		$error = array('message' => 'Не все параметры заданы', 'status' => 400);
		return response()->json($error,400);
	}

	public function addGroupToUser(Request $request) {
		$user = User::where('id',$request->input('id'))->firstOrFail();
		$group = Group::firstOrCreate(['user_id' => $user->id,'group_id'=> $request->input('group_id')]);
		return response('ОК',200);
	}

	public function delGroupFromUser(Request $request) {
		$user = User::where('id',$request->input('id'))->firstOrFail();
		$group = Group::where('user_id', $user->id)->where('group_id', $request->input('group_id'))->firstOrFail();
		$group->delete();
		return response('ОК',200);
	}

	public function setRole(Request $request) {

		if($request->input('id') && $request->input('role_id'))
		{
			$user = User::where('id',$request->input('id'))->firstOrFail();
			$user->role_id = $request->input('role_id');
			return response()->json($user,200);
		}
		$error = array('message' => 'Не все параметры заданы', 'status' => 400);
		return response()->json($error,400);
	}

	public function create(Request $request) {
		if (User::where('login', $request->input('login'))->exists())
			return response()->json(['error' => ['message' => 'Пользователь с таким логином уже существует']] ,409);

		$user = User::create($request->all());
		$user->pass = $this->hashPass($request->input('pass'));
		$user->save();
		return response()->json($user,200);
	}

    public function getUserData (Request $request) {
		$user = Auth::user();
		$user->permissions; // populate relation with trigger getter
        return response()->json($user, 200);
    }

    private function hashPass ($pass) {
        return hash('sha256', $pass);
    }

	public function all () {
		$res = User::all();
		return response()->json($res, 200);
	}

	public function delete (Request $request, $id) {
		$model = User::findOrFail($id);
		$model->delete();
		return response()->json($model, 200);
	}
}
