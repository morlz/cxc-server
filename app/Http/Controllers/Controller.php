<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Group;

class Controller extends BaseController
{
    public function hasAccess($roles)
	{
		if($this->isAdmin() || in_array(Auth::user()->role_id,$roles))
		{
			return true;
		}
		return false;
	}

	public function hasAccessToGroup($groupId) {

		if($this->isAdmin() || Group::where('user_id',Auth::user()->id)->where('group_id',$groupId)) {
			return true;
		}
		return false;
	}

	public function isAdmin() {
		return Auth::user()->role_id == \App\Role::ADMIN;
	}

	public function checkAccess ($name) {
		Auth::user()->checkAccess($name);
	}
}
