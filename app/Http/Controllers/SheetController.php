<?php

namespace App\Http\Controllers;

use App\Sheet;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SheetController extends Controller
{
    public function __construct()
    {
        //
    }

    public function all (Request $request) {
        $res = Sheet::all();

        return response()->json($res, 200);
    }

    public function one (Request $request, $id) {
		$res = Sheet::where('id', $id)->with(['data'])->firstOrFail();

        return response()->json($res, 200);
    }

    public function create (Request $request) {
        $model = Sheet::create($request->all());
        $model->save();
        return response()->json($model, 200);
    }

    public function update (Request $request, $id) {
        $model = Sheet::findOrFail($id);
        $model->update($request->all());
        return response()->json($model, 200);
    }

    public function delete (Request $request, $id) {
        $model = Sheet::findOrFail($id);
        $model->delete();
        return response()->json($model, 200);
    }
}
