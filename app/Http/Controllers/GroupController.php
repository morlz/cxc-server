<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Group;
use App\Sheet;
use App\Speciality;
use \Illuminate\Http\Request;

class GroupController extends Controller
{
	//private $availableSpecialities = array('15','16','12');

	public function getAvailableSpecialities()
	{
		$specs = Speciality::all();
		$result = array();
		foreach ($specs as $spec) {
			$result[] = $spec->code;
		}
		return $result;
	}

    public function one(Request $request, $id) {

		if(!$this->hasAccessToGroup($id)) {
			$error = array('message' => 'Forribden', 'status' => 403);
			return response()->json($error,403);
		}

		$api = app('MoodleApi');
		$res = $api->getGroup($id);
		$code = substr($res->name, 0, 2);
		$res->spec = Speciality::where('code',$code)->firstOrFail();
		$res->users = array();
		$group_members =  $api->getGroupMembers($id);

		foreach($group_members->userids as $member) {
			$userData = $api->getUserData($member);
			$student = array(
				'id'=>$userData->id,
				'name' => str_replace($res->name,'',$userData->fullname)
			);

			array_push($res->users,$student);
		}
		return response()->json($res, 200);
	}

	public function all(Request $request) {

		$availableGroups = Auth::user()->groups()->getResults();

		$gr = array();

		if(Auth::user()->role_id != \App\Role::ADMIN) {
			foreach($availableGroups as $group) {
				array_push($gr,$group->group_id);
			}

			if(!$gr) return response()->json(array(), 200);
		}


		$api = app('MoodleApi');
		$res = $api->getGroups($gr);
		$i = -1;
		foreach($res as $group) {
			$i++;
			if(!in_array(substr($group->name, 0, 2),$this->getAvailableSpecialities()))
			{
				unset($res[$i]);
				continue;
			}
			$group->sheets = Sheet::where('group_id',$group->id)->get();
		}
		return response()->json(array_values($res), 200);
	}

	public function setup (Request $request) {
		return response()->json(Group::all(), 200);
	}

	public function save (Request $request) {
		$items = $request->input('items');
		if (!is_array($items))
			return abort(400, 'Param "items" must be an array');

		Group::truncate();
		Group::insert($items);

		return response()->json($items, 200);
	}
}
