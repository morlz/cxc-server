<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sheet extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'group_id', 'from', 'to'
    ];


	public function group() {
		return $this->hasOne('App\Group');
	}

	public function data() {
		return $this->hasMany('App\SheetData');
	}
}
