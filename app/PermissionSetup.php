<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionSetup extends Model {
	public $timestamps = false;
	protected $table = 'user_permissions';
    protected $fillable = [
        'user_id', 'permission_id'
    ];
}
