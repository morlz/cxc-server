<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SheetData extends Model
{
	public $table = 'sheetdatas';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','user_id', 'sheet_id', 'value', 'date'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
	 protected $dates = [
         'date'
     ];
}
