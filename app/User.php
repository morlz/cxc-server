<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use App\Group;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name', 'login','role_id','pass'
    ];

	public function role()
	{
		return $this->hasOne('App/Role');
	}

	public function groups()
	{
		return $this->hasMany('App\Group');
	}
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'pass','api_token'
    ];

	public function permissions () {
		return $this->belongsToMany('App\Permission', 'user_permissions');
	}

	public function checkAccess ($something) {
		if (!is_array($something))
			return $this->checkAccessItem($something);

		foreach ($something as $key => $value)
			$this->checkAccessItem($something);
	}

	private function checkAccessItem ($item) {
		if (!$this->permissions()->where('name', $item)->exists())
			abort(403, 'Access denied');
	}
}
