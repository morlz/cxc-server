<?php

namespace App;

class Role
{
	const ADMIN = 1;
	const TEACHER = 2;
	const USER = 3;
}
