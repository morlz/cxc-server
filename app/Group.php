<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_id', 'user_id'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

	 // public function users () {
		//  return $this->hasMany('App\User');
	 // }

	 public function sheets () {
		 return $this->hasMany('App\Sheet');
	 }

	 public function users() {
		 return $this->hasMany('App\User');
	 }
}
