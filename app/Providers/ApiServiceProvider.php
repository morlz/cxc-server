<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class ApiServiceProvider
{

	static private $base_url = 'http://lk.dmitrov-dubna.ru';
	static private $web_service_url = '/webservice/rest/server.php';
	static private $login_url = '/login/token.php';
	static private $web_service_token = '78f7f95f815a3d063d8e801def859b68';
	static private $service_name = 'test';
	private $client = null;

	public function __construct()
	{
		$this->client = new \GuzzleHttp\Client(['base_uri' => self::$base_url]);
	}

	public function invoke($method,$url,$params = array(),$data = array()){

		return $this->client->request($method,$url,['query' => $params,'form_params' => $data]);
	}

	public function callFunction($function,$args,$isWebServiceUser = false) {

		$token = $isWebServiceUser ? self::$web_service_token : Auth::user()->api_token;
		$params = array(
			'wstoken' => $token,
			'wsfunction' => $function,
			'moodlewsrestformat' => 'json',
		);
		$response = $this->invoke('post',self::$web_service_url,$params,$args);
		if($response->getStatusCode() === 200){
			return json_decode($response->getBody());
		} else {
			return array();
		}

	}

	public function authenticate($login,$password) {
		$params = array(
			'username' => $login,
			'password' => $password,
			'service' => self::$service_name,
		);
		$response = $this->invoke('GET',self::$login_url,$params);
		if($response->getStatusCode() === 200){
			echo $response->getBody();
			$json = json_decode($response->getBody());
			if(property_exists($json,'token')) {
				return $json->token;
			}
		} else {
			return Null;
		}
	}

	public function getGroups($ids = array()) {
		$data = array('cohortids' => $ids);
		return $this->callFunction('core_cohort_get_cohorts',$data,true);
	}

	public function getGroup($id) {
		$data = array('cohortids' => array($id));
		$res = $this->callFunction('core_cohort_get_cohorts',$data,true);
		if($res) {
			return $res[0];
		}
	}

	public function getGroupMembers($id) {
		$data = array('cohortids' => array($id));
		$res = $this->callFunction('core_cohort_get_cohort_members',$data,true);
		if($res) {
			return $res[0];
		}
	}

	public function getUserData($id) {
		$data = array('criteria' => array(array('key' => 'id', 'value' => $id)));
		$res = $this->callFunction('core_user_get_users',$data,true);
		if($res->users) {
			return $res->users[0];
		}
	}

	public function hasCapability($capability,$user_id) {

	}



}
